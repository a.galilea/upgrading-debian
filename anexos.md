# ANEXO 1
## Instalación y configuración del entorno de pruebas
### Vmware Workstation Player 15
0. Nos aseguramos entrando en la BIOS que el equipo tiene habilitada la virtualización y de que estamos 
utilizando el kernel más reciente para nuestra versión de Linux.  
1. Descargamos la aplicación de la 
[web oficial](https://my.vmware.com/en/web/vmware/free#desktop_end_user_computing/vmware_workstation_player/15_0)  
2. instalamos los siguientes paquetes: 
```
dnf install kernel-headers kernel-devel gcc glibc-headers
dnf -y install elfutils-libelf-devel
```  
3. ejecutamos el archivo de instalación de la aplicación descargado en el paso 1, si es necesario le damos 
permiso de ejecución con `chmod +x`  

Con esto debería ser suficiente, pero lamentablemente Fedora 27 no está entre las distribuciones soportadas 
oficialmente por el software, y puede dar problemas. En nuestro caso esto se traduce en que al intentar ejecutar 
el programa da un error al intentar acceder al módulo vmmon. Una posible solución es la que se da en el paso siguiente. 
Fuentes de esto [aquí](https://github.com/mkubecek/vmware-host-modules) y 
[aquí](https://wiki.archlinux.org/index.php/VMware)  
4. ejecutamos los siguientes comandos:
``` 
cd /usr/lib/vmware/modules/source
sudo tar -xvf vmnet.tar
sudo tar -xvf vmmon.tar
sudo make -C vmmon-only
sudo make -C vmnet-only
sudo mkdir /lib/modules/$(uname -r)/misc
sudo cp vmmon-only/vmmon.ko /lib/modules/$(uname -r)/misc
sudo cp vmnet-only/vmnet.ko /lib/modules/$(uname -r)/misc
sudo depmod -a
sudo vmware-modconfig --console --install-all
```  
Es posible que ni con esto consigamos hacer funcionar el software de virtualización, por lo que adjuntamos 
el procedimiento para instalar un software alternativo y libre, **KVM**.  

### KVM
Extraído de los apuntes de la asignatura *M05 Fonaments de Maquinari* del *CFGS ASIX*. 
[enlace](https://software.intel.com/en-us/blogs/2009/06/25/understanding-vt-d-intel-virtualization-technology-for-directed-io/)  
0. Al igual que en caso de VMWare primero debemos comprobar que la virtualización está habilitada en la BIOS (opciones VT, 
 VT-x y VT-d, no todas estarán presentes). Desde el sistema podemos comprobar si la virtualización está activada usando 
 el comando 
`egrep '^flags.*(vmx|svm)' /proc/cpuinfo`  
Si el comando devuelve algo, significa que la virtualización está activada y podemos seguir, si no devuelve nada es 
que o bien está desactivada o que el sistema no es compatible y no se puede hacer.  
1. Como root, instalamos el grupo de paquetes de virtualización 
`dnf install @virtualization`  
2. Iniciamos el servicio de virtualización y lo habilitamos para que arranque solo al iniciar el sistema
``` 
systemctl start libvirtd
systemctl enable libvirtd 
```  
3. Comprobamos que los módulos del kernel para KVM se han cargado con `lsmod|grep kvm`. Si en la respuesta aparecen 
las cadenas *kvm_amd* o *kvm_intel*, todo va bien, de lo contrario habría que activar la virtualización en la BIOS.
4. El servicio de virtualización ya debería funcionar, para gestionar las máquinas virtuales usamos el programa 
gráfico **virt-manager**, ejecutable desde consola o entorno gráfico. Por defecto, las máquinas virtuales almacenadas 
se crean en el directorio */var/lib/libvirt/images/*.

En principio KVM es capaz de ejecutar Máquinas virtuales de VMWare, en [esta](https://access.redhat.com/articles/1351473) 
web las instrucciones para abrir una máquina virtual de VMWare con KVM. Si aparecen dificultades 
[esta otra](https://www.linuxquestions.org/questions/linux-virtualization-and-cloud-90/converting-vmware-image-using-lvm-to-kvm-869949/) 
web puede ser útil.

# ANEXO 2
## Instalación y configuración de la máquina virtual con Debian 5
Descargamos de [aquí](https://www.debian.org/releases/lenny/debian-installer/) un dvd de instalación de Debian 5 Lenny 
y lo utilizamos para crear una máquina virtual con dicho S.O. Le asignamos 2GB de RAM, un procesador de 1 nucleo, 16 GB 
de disco duro y una tarjeta de red. Al conectar la máquina virtual arrancará desde esta imágen y aparecerá el asistente 
de instalación de Debian.
Seguimos las instrucciones para instalar el S.O. Creamos un usuario **root** con clave *jupiter* y un usuario **aitor** 
con clave *clscls*.  
Una vez instalado el sistema, al entrar por primera vez tenemos que instalar el software que vamos a necesitar, y para 
ello lo primero es configurar los repositorios de Debian. Como el sistema es tan antiguo los repositorios normales han 
sido abandonados, pero queda un repositorio archivo en el que se almacena todo el software de versiones descatalogadas. 
Para utilizarlo, debemos editar el archivo */etc/apt/sources.list*, comentar todas las lineas y añadir la siguiente linea:  
`deb http://archive.debian.org/debian lenny main`  
Una vez hecho esto actualizamos la base de datos de apt con 
`apt-get update`   
Ahora ya podemos empezar a instalar y actualizar los programas de la configuración inicial.
Si por defecto se instala **PostgreSQL**, lo desinstalamos con 
`apt-get autoremove postgresql`  
Instalamos **MySQL 5.0** con `apt-get install mysql-server`, **Apache** y los programas de correo ya se instalan solitos 
de serie, aunque falta algún paquete que añadimos junto con un servidor dns (**bind**), **ssh** y **PHP** con 
```
apt-get install ssh, bind9, courier-imap courier-authlib-mysql courier-pop courier-pop-ssl courier-maildrop courier-mta  
apt-get install php5 php5-mysql libpam-mysql  
apt-get install getmail4 rkhunter sudo gamin  
```   
Para instalar un panel de control web utilizaremos **webmin**, ya que es gratuito y no apetece pagar por dar más 
realismo al proyecto. Para ello tenemos que descargar una versión acorde con la antigüedad del servidor y elegimos la 
1.57, de 2012 (una de las últimas que salieron antes de que Lenny quedara obsoleto). Para descargarlo usamos el 
siguiente 
[enlace](https://sourceforge.net/projects/webadmin/files/webmin/1.570/webmin_1.570_all.deb/download?use_mirror=datapacket).
Para instalarlo usamos `dpkg -i webmin_1.570_all.deb`
Probáblemente dara un error de dependencias, necesitamos instalar primero varios paquetes:
``` 
apt-get install libnet-ssleay-perl libauthen-pam-perl libio-pty-perl apt-show-versions
```
Una vez instaladas las dependencias, si volvemos a ejecutar el comando para instalar Webmin volverá a dar un error de 
dependencias que se soluciona con  
`apt-get -f install`  
Si instalamos primero las dependencias y luego webmin, podemos ahorrarnos éste último paso

Con esto ya tenemos un panel de control con acceso web. Para acceder a nuestro nuevo panel abrimos un navegador web en 
la máquina real e introducimos la siguiente url:
`https://172.16.191.130:10000`  
La IP puede variar, hay que poner en cada caso la IP de la máquina virtual.
  
El comportamiento de Vim es bastante extraño en Debian, para corregirlo y que se pueda usar en condiciones instalamos el 
paquete **vim-nox** usando el comando  
`apt-get install vim-nox`    

Para sincronizar la hora con un servidor externo usamos los paquetes ntp y ntpdate.  
`apt-get install ntp ntpdate`  

Para instalar **phpmyadmin** (usuario root, pass jupiter):  
`apt-get install php5-gd php5-imap phpmyadmin libapache2-mod-fcgid php-auth php5-mcrypt mcrypt php5-imagick \
    libapache2-mod-suphp libruby`  
Una vez instalado este paquete podemos acceder a la base de datos desde la máquina anfitrión introduciendo la URL 
*http://172.16.191.130/phpmyadmin* en un navegador web. Si hemos configurado las dns o el archivo /etc/hosts podemos usar 
un dominio en vez de la IP (en nuestro caso *http://proyecto.net/phpmyadmin*)     

### WORDPRESS
Descargamos la versión 3.4 (23 de junio de 2012) de la [web oficial](https://es.wordpress.org/download/releases/)
La instalación requiere **php5** (instalado anteriormente), un servidor **Apache** (idem) y una base de datos (la 
creamos con los comandos 
```
mysql -p jupiter` 
> create database wordpress;
> exit;
```  
Una vez cumplidos estos prerrequisitos, descomprimimos el contenido del archivo tar en */var/www*. Para poder acceder al 
menú de configuración primero debemos configurar el acceso a la base de datos que acabamos de crear, para ello 
copiamos el archivo de configuración de ejemplo con 
`cp /var/www/wordpresswp-config-sample.php /var/www/wordpress/wp-config.php`  
modificamos el archivo */var/www/wordpress/wp-config.php* y modificamos las siguientes líneas:
```
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wordpress');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'jupiter');
```
y accedemos al directorio desde un navegador web (172.16.191.130/wordpress). Aparecerá el asistente de instalación para 
la página web, lo seguimos y ya tenemos web.
Usuario wordpress: admin
password: jupiter
mail: admin@proyectoedt.net

Para poder acceder a la nueva web desde cualquier sitio habría que configurar un servidor DNS, nosotros nos conformaremos 
con modificar el archivo /etc/hosts de la máquina anfitrión para hacer pruebas.
``` 
echo "172.16.191.130  proyecto.net www.proyecto.net www.proyectoedt.net">>/etc/hosts
```  

### COURIER
Para configurar el servidor de correo nos hemos basado en [esto](http://www.daeglo.com/projects/couriermailserversetup) 
y [esto](https://www.howtoforge.com/perfect-server-debian-squeeze-with-bind-and-courier-ispconfig-3-p4)
```
# creamos usuario administrador de correo, sysadm1 pwd jupiter, uid/gid 1001
useradd -p jupiter sysadm1   
# como root, creamos un directorio para la base de datos de usuarios y la creamos
cd /etc/courier/
mkdir userdb && chmod 0700 userdb
/etc/init.d/courier-authdaemon restart
# creamos usuario de correo y le asignamos buzón de correo
/usr/sbin/userdb proyecto.net/sisadmin@proyecto.net set home=/var/mail/proyecto.net/xmpl uid=1001 gid=1001
/usr/sbin/userdbpw -md5|/usr/sbin/userdb proyecto.net/sisadmin@proyecto.net set systempw
	# (pwd: jupiter)
# creamos el buzón de correo para el usuario y le asignamos la propiedad y los permisos adecuados
mkdir -p /var/mail/proyecto.net/xmpl
/usr/bin/maildirmake /var/mail/proyecto.net/xmpl/Maildir
chown -R 1001:1001 /var/mail/proyecto.net/xmpl
/usr/sbin/makeuserdb
```
------  
Alternativa, usar MySQL como gestor de la base de datos de correo (no usado en nuestro caso)
Configuración de la base de datos del correo:
```
# creamos un usuario y grupo unix para Courier
groupadd courierdb
useradd -g courierdb courierdb
# creamos un directorio para la base de datos y la asignamos al usuario de la base de datos
mkdir /var/spool/courierdb
chown courierdb:courierdb /var/spool/courierdb
```  
Creamos la base de datos y la tabla en la que guardaremos los datos de usuarios  
```
mysql -p jupiter
mysql> CREATE USER courierdb IDENTIFIED BY ‘jupiter’;
mysql> CREATE DATABASE courierdb;
mysql> GRANT usage on *.* TO courierdb@localhost IDENTIFIED BY ‘jupiter’;
mysql> GRANT all privileges on courierdb.* TO courierdb@localhost;
mysql> CREATE TABLE mail_users (
    -> id INT AUTO_INCREMENT,
    -> username VARCHAR(64) NOT NULL,
    -> vhost VARCHAR(64) NOT NULL,
    -> passwd VARCHAR(64) NOT NULL,
    -> maildir VARCHAR(256),
    -> alias VARCHAR(32)
    -> PRIMARY KEY (id));
mysql> exit;
```  
Modificamos la configuración de Courier para especificar que MySQL contiene la base de usuarios
```
vim /etc/courier/authdaemonrc

 authmodulelist="authmysql"
 
 MYSQL_SERVER            localhost
 MYSQL_USERNAME          postfix
 MYSQL_PASSWORD          postfix
 MYSQL_SOCKET            /var/run/mysqld/mysqld.sock
 MYSQL_PORT              3306
 MYSQL_DATABASE          courierdb
 MYSQL_USER_TABLE        mail_users
 MYSQL_CRYPT_PWFIELD     passwd
 # the numerical userid of the postfix account
 MYSQL_UID_FIELD         '1002'
 #the numerical groupid of the postfix account
 MYSQL_GID_FIELD         '1002'
 MYSQL_LOGIN_FIELD       username
 # the location of the mailboxes on the server
 # Please change this is you are going to use a different location.
 #MYSQL_HOME_FIELD        '/usr/local/virtual'
 # The user's name (optional)
 #MYSQL_NAME_FIELD        name
 # The location where the user mailbox is defined in the table.
 MYSQL_MAILDIR_FIELD     maildir
``` 
Más información sobre este proceso 
[aquí](https://switch.richard5.net/installing-the-mailserver/the-basic-mailserver/configuring-courier-auth-to-use-mysql/index.html)  

Independientemente de qué tipo de base de datos usemos, editamos el archivo que contiene los dominios que se gestionan 
como correo local  
``` 
vi /etc/courier/locals
    localhost
    oldfaithful.localdomain 
```  
Editamos el archivo que contiene los dominios que se gestionan como correo SMTP  
```
vi /etc/courier/esmtpacceptmailfor.dir/domains
    proyecto.net
```
Los correos con destino a cuentas de correo virtuales se configuran en hosteddomains/* (nosotros no tenemos de estas)

Por último, desactivamos la resolución de nombres para no ralentizar el procesado de correo y activamos la autenticación
```
vi esmtpd
    TCPDOPTS="-stderrlogger=/usr/sbin/courierlogger -noidentlookup -nodnslookup"
    ESMTPAUTH="LOGIN CRAM-MD5"
    AUTH_REQUIRED=1
```  
Reiniciamos el servicio /etc/init.d/courier-authdaemon restart y lo testeamos con telnet  
``` 
telnet localhost 25
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
220 oldfaithful ESMTP
EHLO testing.example.com
250-oldfaithful Ok.
250-XVERP=Courier
250-XEXDATA
250-XSECURITY=NONE,STARTTLS
250-PIPELINING
250-8BITMIME
250-SIZE
250 DSN
MAIL From: <testing@example.com>
250 Ok.
RCPT To: <aitor@oldfaithful.localdomain>
250 Ok.
DATA
354 Ok.
Date: May 1 2019 15:05:59
From: testing@example.com
To: aitor@oldfaithful.localdomain
Subject: test de correo
probando probando 123
.
250 Ok. 5CBE547C.000026C1
QUIT
221 Bye.
Connection closed by foreign host.
```
Comprobamos que el mensaje está en el directorio de correo del usuario al que se lo hemos enviado
``` 
less /home/aitor/Maildir
From testing@example.com Tue Apr 23 01:57:10 2019
Delivered-To: aitor@oldfaithful.localdomain
Return-Path: <testing@example.com>
Received: from testing.example.com ([::ffff:127.0.0.1])
  by oldfaithful with esmtp; Tue, 23 Apr 2019 01:54:27 +0200
  id 000CD35C.5CBE547C.000026C1
Date: May 1 2019 15:05:59
From: testing@example.com
To: aitor@oldfaithful.localdomain
Subject: test de correo
probando probando 123
/home/aitor/Maildir (END) 
```  
También podemos instalar un programa de correo como mutt, `apt-get install mutt`.
