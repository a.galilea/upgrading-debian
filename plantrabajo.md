# PLAN DE TRABAJO
| fecha  | objetivo                 | horas |
| ------ | ------------------------ | ----- |
| 26-abr | preparar máquina virtual | 30    |
| 02-may | subir debian 5 a 6       | 20    |
| 08-may | subir debian 6 a 7       | 20    |
| 15-may | subir a 64 bits y Deb 8  | 30    |
| 20-may | completar documentación  | 20    |
| 26-may | imprevistos/ debian 9    | 30    |

---
# DIETARIO
#### DIA 32

#### DIA 31
Correcciones guía de actualización
#### DIA 30
Presentación
#### DÍA 29
Presentación
#### DÍA 28
Presentación
#### Día 27
Presentación
#### Día 26
Presentación
#### Día 25
Preparación de la presentación.
#### Día 24
Preparación de la presentación. Documentar
#### Día 23
Preparación de la presentación.
#### Día 22
Preparación de la presentación. Arreglar problemas con Apache
#### Día 21
Preparación de la presentación
#### Día 20
Documentación, comenzamos con la preparación de la presentación
#### Día 19
Documentación, pruebas con Apache, pruebas Pandoc para presentación
#### Día 18
Documentación
#### Día 17
Pruebas con KVM, documentación
#### Día 16
Resolver problemas con la actualización de Apache
#### Día 15
Documentar, resolver problemas con la actualización de Apache
#### Día 14
Documentar, trabajar en actualización a Debian 8
#### Día 13
Documentar, comenzar actualización a Debian 8
#### Día 12
Documentar
#### Día 11
Instalar y configurar software de virtualización en clase (VMWare Player 15). Investigar sobre kvm
#### Día 10
Perder parte del avance del dia anterior, rehacerlo y completar la actualización a Debian 64 bits, comprobar el funcionamiento
#### Día 9
Completar actualización a Debian 7 32 bits, hacer comprobaciones, comenzar la actualización a Debian 7 64 bits.
#### Día 8.
Actualizar Webmin y Wordpress.Guardar copia del estado final de Debian 6, que será la base para comenzar a actualizar a Debian 7 32 bit. Comenzar actualización a Debian 7.
#### Día 7.
Configurar Courier.
#### Día 6. 
Configurar Courier, actualizar a Debian 6 (falta Wordpress y Webmin), guardada copia de la máquina virtual en Mega.
#### Día 5. Instalación de servicios
Configurar Courier, crear una web con Wordpress
#### Día 4. Instalación de servicios
Instalar y configurar Apache y Courier
#### Día 3. Instalación de Debian
Instalar Debian 5, configurar el repositorio para apt-get e instalar los paquetes básicos.
#### Día 2. Selección de software de virtualización
Terminar configuración VMWare Player 15, optamos por usar este software de virtualización porque Virtual Box obliga a 
especificar si la máquina a emular es de 32 o 64 bits y ésto podría causar problemas cuando hagamos la transición de 32 
a 64 bits.
#### Día 1. Instalación de software de virtualización
Instalar y configurar VMWare Player 15 y Virtual Box 6.0
