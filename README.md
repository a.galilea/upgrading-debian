# UPGRADING DEBIAN

Proyecto fin de curso 2018-19 ASIX@EDT de Aitor Galilea. Actualización de un servidor obsoleto con debian 5 Lenny hasta una versión con 
soporte (Debian 8 o 9)  
## TABLA DE CONTENIDOS
- **anexos.md**: archivo con información sobre cómo crear una máquina virtual con 
la configuración inicial
- **aux**: directorio con imágenes y elementos auxiliares utilizados en este y otros documentos
- **documentación**: directorio con manuales y documentación oficial utilizados en el proyecto
- **guiaactualizacion.md**: manual técnico de actualización 
- **logs**: directorio con archivos de log y registros de cambios del servidor
- **presentacion.html**: archivo con la presentación del proyecto 
- **plantrabajo.md**: planificación de tareas y dietario de actividades
- **README.md**: este documento
## DESCRIPCIÓN DEL PROYECTO
En informática se suele decir "si funciona, no lo toques", lo que nos lleva frecuentemente a encontrarnos con problemas 
debidos a hardware y software muy obsoletos. El objetivo de este proyecto es intentar rescatar un servidor que no ha sido 
actualizado en muchos años y, tanto si se consigue como si no,valorar si merece el esfuerzo frente a una migración
(traspasar los datos a otro servidor y reinstalar el servidor obsoleto desde cero a una versión actual).  

Como es lógico, los resultados alcanzados dependen de la situación inicial y sólo serán aplicables a nuestro caso de 
prueba, pero el procedimiento seguido es extrapolable a otras configuraciones similares. 

Se puede encontrar copias de cada fase del proceso de la máquina virtual empleada [aquí](https://mega.nz/#F!FZ9TTCrb!9Xf9ajp85UGgYp4ZSgLRIQ)
## CONFIGURACIÓN INICIAL   
Disponemos de una máquina virtual creada con vmware player con las siguientes características:
- 2GB RAM
- 1 procesador con un núcleo 
- 16 GB de disco duro
- lector de CD/DVD 
- 1 adaptador de red configurado en modo NAT

en esta máquina virtual hemos instalado:
- Sistema operativo Debian 5 Lenny (publicado en 14/02/2009, soportado hasta 06/02/2012)
- kernel 2.6.26-2-686
- Apache 2.2.9
- PHP 5.2.6
- MySQL 5.0.59
- Postfix 2.5.5
- Courier-imap 4.4.0-2
- Perl 5.10.0-19
- Python 2.5.2
- Webmin 1.570

Este servidor está configurado para servir una página web creada con Wordpress y gestionar una cuenta de correo.
